import React, { Component } from 'react';
import * as moment from "moment";

class Clock extends Component {

    constructor(props) {
        super(props);
        this.state = {
            days: 0,
            hours: 0,
            minutes: 0, 
            seconds: 0
        }
        console.log(props);
    }

    componentWillMount() {
        this.getTimeUntil();    
    }
    
    componentDidMount() {
        setInterval(() => { this.getTimeUntil(this.props.deadline)}, 1000);
    }

    getTimeUntil(deadline) {
        const time = Date.parse(deadline) - Date.parse(new Date());
        var tempTime = moment.duration(time);
        const days = Math.floor(tempTime.asDays());
        const hours = Math.floor(tempTime.hours());
        const minutes = Math.floor(tempTime.minutes());
        const seconds = Math.floor(tempTime.seconds());
        this.setState({days , hours, minutes, seconds});
    }

    render() {
        return (
            <div>
                <div className="inline date-item">
                    {this.state.days} days
                    </div>
                <div className="inline date-item">
                    {this.state.hours} hours
                    </div>
                <div className="inline date-item">
                    {this.state.minutes} minutes
                    </div>
                <div className="inline date-item">
                    {this.state.seconds} seconds
                </div>
            </div>
        );
    }
}

export default Clock;