import React, { Component } from 'react';
import './App.css';
import Clock from './Clock';
import { Form, FormControl } from "react-bootstrap";
import moment from 'moment';
import DatePicker from 'react-datepicker';
import 'react-datepicker/dist/react-datepicker.css';

class App extends Component {

    constructor(props) {
        super(props);
        this.state = {
            deadline: moment('12/25/2018')
        }
        this.handleChange = this.handleChange.bind(this);
    }

    handleChange(date) {
        this.setState({
            deadline: date
        });
    }

    render() {

        return (

            <div className="App">
                <div className="countdown">
                    Countdown to {moment(this.state.deadline).format('MMMM DD, YYYY')}
                </div>
                <Clock deadline={this.state.deadline} />
                <Form inline>
                    <DatePicker
                        selected={this.state.deadline}
                        onChange={this.handleChange}
                    />

                    {/* <FormControl placeholder="new deadline" /> */}
                    <button className="form-control" type="submit" onClick={() => { this.changeDeadline(); }}>Submit</button>
                </Form>
            </div>
        );
    }
}

export default App;